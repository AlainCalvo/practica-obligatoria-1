using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallVault : MonoBehaviour
{
    #region Variables

    GameManager gm;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    #endregion

    #region Detections

    private void OnTriggerEnter(Collider other)
    {
        //Sets win condition on true
        if (other.gameObject.name == "ShootingPoint") gm.victory = true;
    }

    #endregion
}
