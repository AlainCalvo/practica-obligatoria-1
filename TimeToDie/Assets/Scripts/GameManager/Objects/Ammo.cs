     using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    #region Variables

    GameManager gm;

    #endregion

    #region Main Methods
    private void Start()
    {
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        //Rotate the object to make an animation
        transform.Rotate(0, 1, 0, Space.Self);
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 3)
        {
            //Adds ammo for player
            gm.ammo = gm.ammo + 8;
            gm.moreAmmo = true;
            Destroy(gameObject);
        }      
    }

    #endregion+
}
