using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    #region Variables

    GameManager gm;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    void Update()
    {
        //Simulates a small animation
        transform.Rotate(0, 1, 0);
    }

    #endregion

    #region Detection Methods

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            //Enables player ability
            transform.Rotate(0, 100, 0);
            transform.Translate(0, 3, 0);
            Destroy(this.gameObject);
            gm.powered = true;
        }        
    }

    #endregion
}
