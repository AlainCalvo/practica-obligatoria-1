using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    #region Variables

    Timer time;

    #endregion

    #region Main Methods

    private void Start()
    {
        time = GameObject.FindObjectOfType<Timer>();
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter(Collider other)
    {
        //Subtract time when cannonball strickes the player
        if (other.gameObject.name == "Player")
        {
            time.SubTract(10);
        };
    }

    #endregion
}
