using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricPanels : MonoBehaviour
{
    #region Variables

    public bool shoot;

    #endregion

    #region Main Methods

    private void Update()
    {
        if (shoot) Destroy(this.gameObject);
    }

    #endregion
}

