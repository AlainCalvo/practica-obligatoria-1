using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTheBox : MonoBehaviour
{
    #region Variables

    GameObject[] electric;
    GameObject joint;

    #endregion

    #region Main Methods
    private void Start()
    {
        joint = GameObject.Find("Joints");
    }

    private void Update()
    {
        //When all electric panels are destroyed the box falls on tank to defeat it
        electric = GameObject.FindGameObjectsWithTag("Electric");
        if (electric.Length <= 0) joint.SetActive(false);
    }

    #endregion
}
