using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISave : MonoBehaviour
{
    #region Variables

    GameObject[] gama;
    GameObject erase;

    #endregion


    #region Main Methods

    private void Awake()
    {
        gama = GameObject.FindGameObjectsWithTag("UI");
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        if (gama.Length > 1)
        {
            erase = gama[1];
            Destroy(erase);
        }
    }

    #endregion
}
