using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    #region variables    

    public Text timeText;
    public Text plusText;
    public Text subText;
    public GameManager gm;

    float hitStrike;
    public float time = 0.0f;
    bool adding;
    bool subtracting;
    public bool skillActive;

    Timer[] timer;
    #endregion

    #region Main Methods

    private void Awake()
    {
        DontDestroyOnLoad(this);
        timer = FindObjectsOfType<Timer>();
    }

    private void Start()
    {
        if (timer.Length > 1) Destroy(timer[1]);
        gm = GameObject.FindObjectOfType<GameManager>();
    }
    private void Update()
    {
        if (gm.gameOver == false && gm.victory == false)
        {
            if (skillActive == false)
            {
                timeText.text = "" + time.ToString("f0");
                time -= Time.deltaTime;
                if (time < 0)
                {
                    timeText.text = "";
                    gm.gameOver = true;
                }
                
                if (subtracting) StartCoroutine(MinusTime());
            }

            if (adding) StartCoroutine(PlusTime());

            if (skillActive) StartCoroutine(Skill());
        }        
    }

    #endregion


    #region Custom Methods

    /// <summary>
    /// Stop the time when player use the ability
    /// </summary>
    /// <returns></returns>
    public IEnumerator Skill()
    {
        yield return new WaitForSeconds(10);
        skillActive = false;
    }

    /// <summary>
    /// Displays on screen the time obtained when the enemy dies
    /// </summary>
    /// <returns></returns>
    public IEnumerator PlusTime()
    {
        plusText.text = "+5";
        yield return new WaitForSeconds(2);
        plusText.text = "";
        adding = false;
    }

    /// <summary>
    /// Displays on screen the time losed when enemy hits player
    /// </summary>
    /// <returns></returns>
    public IEnumerator MinusTime()
    {
        subText.text = "-" + hitStrike.ToString("f0");
        yield return new WaitForSeconds(.5f);
        subText.text = "";
        subtracting = false;
    }

    /// <summary>
    /// Adds time to the timer
    /// </summary>
    public void AddTime()
    {
        time += 5f;
        adding = true;
    }

    /// <summary>
    /// Subtracts timer from the timer
    /// </summary>
    /// <param name="strike"></param>
    public void SubTract(float strike)
    {
        hitStrike = strike;
        time -= strike;
        subtracting = true; 
    }

    #endregion
}
