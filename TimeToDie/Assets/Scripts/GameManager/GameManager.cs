using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    #region variables

    public bool gameOver;
    public bool victory;

    public GameObject fail;
    public GameObject win;
    
    PlayerController player;    

    [Header("---Scene Management---")]

    CountingEnemies numEnem;

    public int nextScene = 0;

    [Header("---Ammonation---")]

    public int ammo = 20;
    public Text bulletsText;
    public Text extrAmmo;
    public bool moreAmmo;

    [Header("---Enemies---")]

    GameObject[] soldiers;
    GameObject[] kamikazes;
    int enemies;
    public Text numberEnemyText;

    [Header("---Power UP---")]

    Timer time;
    public GameObject skillText;
    public bool powered;

    GameObject[] gama;
    GameObject erase;

    #endregion

    #region Main Methods

    private void Awake()
    {
        gama = GameObject.FindGameObjectsWithTag("GameManager");
    }

    private void Start()
    {
        if(gama.Length > 1)
        {
            erase = gama[1];
            Destroy(erase);
        }
        
        player = GameObject.FindObjectOfType<PlayerController>();
        time = GameObject.FindObjectOfType<Timer>();
    }

    private void Update()
    {
        ShowCursor();
        if (gama.Length > 1) Destroy(gama[1]);       
        NextStage();
        RemainingEnemies();
        AmmoManagement();
        Skill();
        GameOver();
        Winner();
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Load next scene whe all enemies are defeated
    /// </summary>
    void NextStage()
    {
        numEnem = GameObject.FindObjectOfType<CountingEnemies>();

        if (numEnem == null)
        {
            if(SceneManager.GetActiveScene().buildIndex < 2) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    /// <summary>
    /// Manage ammo info on screen
    /// </summary>
    void AmmoManagement()
    {
        if (ammo > 0)
        {
           player.loadedGun = true;
           bulletsText.text = "" + ammo.ToString();
        }

        else if (ammo == 0) 
        {
            player.loadedGun = false;
            bulletsText.text = "NO AMMO";
        }
        if (moreAmmo) StartCoroutine(ExtraAmmo());
    }   

    /// <summary>
    /// Display on screen the ammo obtained
    /// </summary>
    /// <returns></returns>
    public IEnumerator ExtraAmmo()
    {
        extrAmmo.text = "+8";
        yield return new WaitForSeconds(2);
        extrAmmo.text = "";
        moreAmmo = false;
    }

    /// <summary>
    /// Manage the use of the ability
    /// </summary>
    void Skill()
    {
        if (powered)
        {
            skillText.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                time.skillActive = true;
                skillText.SetActive(false);
                powered = false;
            }
        }
    }

    /// <summary>
    /// Manages the remaining enemies account
    /// </summary>
    void RemainingEnemies()
    {
        soldiers = GameObject.FindGameObjectsWithTag("Soldier");
        kamikazes = GameObject.FindGameObjectsWithTag("Kamikaze");
        enemies = soldiers.Length + kamikazes.Length;
        numberEnemyText.text =("Enemies Remaining: ") + enemies.ToString("0");
    }

    /// <summary>
    /// Manage the visibility of the cursor
    /// </summary>
    void ShowCursor()
    {
        if (gameOver || victory) Cursor.lockState = CursorLockMode.None;
        else Cursor.lockState = CursorLockMode.Locked;
    }

    /// <summary>
    /// Restart the game
    /// </summary>
    public void Restart()
    {
        gameOver = false;
        victory = false;
        SceneManager.LoadScene(0);
        time.time = 200;
        ammo = 25;
    }

    /// <summary>
    /// Close the game
    /// </summary>
    public void LeaveGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Open Game over menu
    /// </summary>
    void GameOver()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            fail.SetActive(true);
        }

        else
        {
            Time.timeScale = 1;
            fail.SetActive(false);
        }
    }

    /// <summary>
    /// Open victory menu
    /// </summary>
    void Winner()
    {
        if (victory)
        {
            Time.timeScale = 0;
            win.SetActive(true);
        }

        else
        {
            Time.timeScale = 1;
            win.SetActive(false);
        }
    }

    #endregion
}

