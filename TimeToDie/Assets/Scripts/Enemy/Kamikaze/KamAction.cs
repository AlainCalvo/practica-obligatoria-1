using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KamAction : MonoBehaviour
{
    #region variables
    [Header("---Classes---")]

    Timer time;
    GameManager gm;
    GameObject target;
    CharacterController cc;

    [Header("---Movement---")]

    float persecDistant = 15;

    Quaternion rot;
    Vector3 lookPos;

    [Header("---Attack---")]

    public bool suicide;
    public float kaboom = 1.85f;
    public GameObject explosion;
    #endregion

    #region Main Methods
    private void Start()
    {
        target = GameObject.Find("Player");
        cc = GetComponent<CharacterController>();  
        time = GameObject.FindObjectOfType<Timer>();
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    private void FixedUpdate()
    {
        if (suicide && gm.victory == false && gm.gameOver == false)
        {
            //Once the player is detected, he moves to the attack distance.
            if (Vector3.Distance(transform.position, target.transform.position) < persecDistant)
            {
                lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                rot = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, 3);
                cc.Move(transform.forward * 5 * Time.deltaTime);
            }

            //attack the player
            if (Vector3.Distance(transform.position, target.transform.position) < kaboom)
            {
                explosion.SetActive(true);
                Destroy(this.gameObject);
                if(time.skillActive == false) time.SubTract(8);
            }
        }
    }

    #endregion
}
