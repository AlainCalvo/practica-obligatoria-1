using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KamGFXController : MonoBehaviour
{
    #region Variables

    [Header("---Classes---")]

    Timer em;
    GameManager gm;
    KamAction kamAct;

    [Header("--GFX Control--")]

    public GameObject full;
    public GameObject final;
    public GameObject explosion;

    [Header("--Health--")]

    public float health = 2;

    #endregion

    #region Main Methods

    private void Start()
    {
        kamAct = GetComponent<KamAction>();
        em = GameObject.FindObjectOfType<Timer>();
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Control the life bar
    /// </summary>
    /// <param name="hurt"></param>
    public void TakeDamage(float hurt)
    {
        if (gm.victory == false && gm.gameOver == false)
        {
            health -= hurt;
            if (health == 1)
            {
                full.SetActive(false);
                final.SetActive(true);
            }
            if (health < 1)
            {
                explosion.SetActive(true);
                Destroy(gameObject,0.1f);
                em.AddTime();
            }
        }       
    }

    #endregion
}
