using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    #region variables
    [Header ("---Classes---")]

    CharacterController cc;
    GameObject target;
    SoldierAction sa;
    KamAction ka;

    [Header ("---Movement---")]

    int routine;

    float persecDistant = 15;
    
    float chrono;
    float grade;

    Quaternion angle;

    #endregion


    #region Main Methods

    private void Start()
    {
        target = GameObject.Find("Player");
        cc = GetComponent<CharacterController>();
        sa = GetComponent<SoldierAction>();
        ka = GetComponent<KamAction>();
    }

    private void Update()
    {
        //Moves the enemy randomly
        if (Vector3.Distance(transform.position, target.transform.position) > persecDistant)
        {
            chrono += 1 * Time.deltaTime;
            if (chrono >= 2.5f)
            {
                routine = Random.Range(0, 2);
                chrono = 0;
            }

            switch (routine)
            {
                case 0:

                    break;

                case 1:
                    grade = Random.Range(0, 360);
                    angle = Quaternion.Euler(0, grade, 0);
                    routine++;
                    break;

                case 2:
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, angle, .5f);
                    cc.Move(transform.forward * 2 * Time.deltaTime);
                    break;
            }
        }

        //Detect the player
        if (Vector3.Distance(transform.position, target.transform.position) < persecDistant)
        {
            if (this.gameObject.tag == "Soldier")
            {
                sa.active = true;
            }

            else if (this.gameObject.tag == "Kamikaze")
            {                
                ka.suicide = true;
            }
        }
    }

    #endregion


    #region Detection Methods   

    private void OnTriggerEnter(Collider other)
    {
        //Redirects the enemy if it collides with the environment
        if (other.gameObject.layer != 8 && other.gameObject.layer != 9)
        {            
            Vector3 rot = transform.rotation.eulerAngles;
            rot = new Vector3(rot.x, rot.y + 180, rot.z);
            transform.rotation = Quaternion.Euler(rot);
            cc.Move(transform.forward * 2 * Time.deltaTime);
        }
    }

    #endregion
}