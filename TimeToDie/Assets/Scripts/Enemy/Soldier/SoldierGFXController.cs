using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierGFXController : MonoBehaviour
{
    #region variables

    [Header ("---Classes---")]
    Timer em;
    GameManager gm;
    SoldierAction soldAct;

    [Header("--GFX Control--")]
    public GameObject full;
    public GameObject mid;
    public GameObject final;

    [Header("--Health--")]

    public float health = 3f;

    #endregion


    #region Main Methods

    private void Start()
    {
        soldAct = GetComponent<SoldierAction>();
        em = GameObject.FindObjectOfType<Timer>();
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    #endregion


    #region Custom Methods

    /// <summary>
    /// Control Soldier�s life bar
    /// </summary>
    /// <param name="hurt"></param>
    public void TakeDamage(float hurt)
    {
        if (gm.victory == false && gm.gameOver == false)
        {
            health -= hurt;

            if (health == 2)
            {
                full.SetActive(false);
                mid.SetActive(true);
            }
            if (health == 1)
            {
                mid.SetActive(false);
                final.SetActive(true);
            }
            if (health < 1)
            {
                Destroy(gameObject);
                em.AddTime();
            }
        }        
    }

    #endregion










}
