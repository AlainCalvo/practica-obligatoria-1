using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierAction : MonoBehaviour
{
    #region variables
    [Header("---Classes---")]

    Timer time; 
    GameManager gm;
    GameObject target;
    PlayerController pc;
    CharacterController cc;

    [Header("---Movement---")]
        
    float persecDistant = 15;
    float attackDistant = 9;
    float chrono;   

    Quaternion rot;
    Vector3 lookPos;

    [Header ("---Attack---")]

    public float range = 6.5f;
    public bool active;

    Transform shootPoint;
    #endregion


    #region Main Methods

    private void Start()
    {
        target = GameObject.Find("Player");
        cc = GetComponent<CharacterController>();
        time = GameObject.FindObjectOfType<Timer>();
        gm = GameObject.FindObjectOfType<GameManager>();
        shootPoint = gameObject.transform.Find("ShootingPoint");
    }

    private void FixedUpdate()
    {
        if (active && gm.victory == false && gm.gameOver == false)
        {
            //Once the player is detected, he moves to the attack distance.
            if (Vector3.Distance(transform.position, target.transform.position) < persecDistant && Vector3.Distance(transform.position, target.transform.position) > attackDistant)
            {
                lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                rot = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, 10);
                cc.Move(transform.forward * 3.2f * Time.deltaTime);
            }

            //attack to player
            else if (Vector3.Distance(transform.position, target.transform.position) <= attackDistant)
            {                
                lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                rot = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, 3);
                chrono += 1 * Time.deltaTime;
                if (chrono >= 1)
                {
                    Attack();
                    chrono = 0;
                }
            }
        }
    }

    #endregion


    #region Custom Methods

    /// <summary>
    /// Shoots to player
    /// </summary>
    void Attack()
    { 
        RaycastHit hit;

        if (Physics.Raycast(shootPoint.transform.position, shootPoint.transform.forward, out hit, range))
        {
            if (hit.collider.gameObject.layer == 3)
            {
                pc = hit.transform.GetComponent<PlayerController>();
                if (pc != null && time.skillActive == false) time.SubTract(3); //Subtract time
            }
        }
    }

    #endregion
}
