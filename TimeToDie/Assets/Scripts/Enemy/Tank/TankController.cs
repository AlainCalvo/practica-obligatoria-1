using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    #region Variables

    Vector3 lookPos;
    Quaternion rot;

    GameObject target;
    GameManager gm;

    #endregion


    #region Main Methods

    private void Start()
    {
        target = GameObject.Find("Player");
        gm = GameObject.FindObjectOfType<GameManager>();
    }
    private void Update()
    {
        if (gm.victory == false && gm.gameOver == false )
        {
            //Look to player
            lookPos = target.transform.position - transform.position;
            lookPos.y = 0;
            rot = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, 3);
        }        
    }

    #endregion
}
