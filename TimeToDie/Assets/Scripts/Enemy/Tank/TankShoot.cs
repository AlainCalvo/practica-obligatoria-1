using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShoot : MonoBehaviour
{
    #region Variables

    Rigidbody rbBall;
    GameManager gm;

    public GameObject cannonBall;
    public Transform shooter;

    float chrono = 0;

    Vector3 currentRot;
    Vector3 position;

    #endregion


    #region Main Methods
    private void Start()
    {
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    void Update()
    {
        if (gm.victory == false && gm.gameOver == false) 
        {
            currentRot = shooter.transform.position;
            position = transform.position;

            chrono += 1 * Time.deltaTime;

            if (chrono >= 1.5f) Shoot();            
        } 
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Shoot a cannonball
    /// </summary>
    void Shoot()
    {
        GameObject go = Instantiate(cannonBall, position, Quaternion.identity);
        rbBall = go.GetComponent<Rigidbody>();
        rbBall.transform.rotation = shooter.transform.rotation;
        rbBall.AddRelativeForce(go.transform.forward * 30, ForceMode.Impulse);
        Destroy(go, 3f);
        chrono = 0;
    }

    #endregion
}
