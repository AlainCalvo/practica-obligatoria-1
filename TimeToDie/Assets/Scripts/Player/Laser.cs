using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    #region Variables

    Collider target;

    #endregion


    #region Main Methods

    private void Update()
    {
        if (target == null) this.gameObject.GetComponent<Renderer>().material.color = Color.white; 
    }

    #endregion


    #region Detection Methods

    //Change laser color when the bullet can impact on enemy
    private void OnTriggerEnter(Collider other)
    {
        target = other;        

        if (other != null && other.gameObject.tag == "Soldier" || other != null && other.gameObject.tag == "Kamikaze" ||other != null && other.gameObject.tag == "Electric")
        {            
            if (gameObject != null) this.gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        this.gameObject.GetComponent<Renderer>().material.color = Color.white;
    }

    #endregion
}



