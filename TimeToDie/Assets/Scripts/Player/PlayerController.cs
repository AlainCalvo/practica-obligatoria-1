using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    #region variables

   [Header("---Classes---")]

    CharacterController cc;
    GameManager gm;
    Timer looseLife; 

    [Header("---Movement---")]

    public float walkSpeed = 2;
    public float sprint = 3.5f;
    float speed;

    float gravity = -9.81f;
    Vector3 velocity; 
    
    float rotX;
    float rotSpeed = 50;

    [Header("---Shoot---")]

    public float range = 5.5f;
    float damage = 1f;

    public bool loadedGun;

    SoldierGFXController target;
    KamGFXController kamikaze;
    ElectricPanels ep;
       
    [Header("---Health---")]

    public bool impact;

    #endregion


    #region Main Methods
    private void Awake()
    {
        cc = GetComponent<CharacterController>();
        gm = GameObject.FindObjectOfType<GameManager>();
        looseLife = GameObject.FindObjectOfType<Timer>();        
    }

    private void Update()
    {
        //Enables firing
        if (gm.ammo > 0) loadedGun = true;
        else loadedGun = false;

        if (gm.victory == false && gm.gameOver == false)
        {
            Movement();
            if (Input.GetMouseButtonDown(0) && loadedGun == true)
            {
                Shoot();
                gm.ammo = gm.ammo - 1;
            }
        }      
    }

    #endregion


    #region Custom Methods

    /// <summary>
    /// Control the movement of player
    /// </summary>
    void Movement()
    {       
        //Horizontal movement
        float MovX = Input.GetAxisRaw("Horizontal");
        float MovY = Input.GetAxisRaw("Vertical");
        Vector3 dir = transform.right * MovX + transform.forward * MovY;
        dir = dir.normalized;

        cc.Move(dir * speed * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;
        cc.Move(velocity * Time.deltaTime);

        //Horizontal vision
        rotX = Input.GetAxis("Mouse X");
        if (rotX != 0) transform.Rotate(Vector3.up * rotX * rotSpeed * Time.deltaTime);

        //Sprint
        if (Input.GetKey(KeyCode.LeftShift) && MovY == 1) speed = sprint;
        else speed = walkSpeed;
    }

    /// <summary>
    /// Control impacts on enemy
    /// </summary>
    void Shoot()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward,out hit, 5))
        {          
            target = hit.transform.GetComponent<SoldierGFXController>();
            if (target != null) target.TakeDamage(damage);            
            
            kamikaze = hit.transform.GetComponent<KamGFXController>();
            if (kamikaze != null) kamikaze.TakeDamage(damage);
            
            ep = hit.transform.GetComponent<ElectricPanels>();
            if (ep != null) ep.shoot = true;
        }
    }

    #endregion
}
