using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewController : MonoBehaviour
{
    #region variables
    GameManager gm;

    float rotX;

    Vector3 currentRot;

    public Transform player;
    #endregion


    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        currentRot = transform.eulerAngles;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        //Controls vertical vision
        if (gm.victory == false && gm.gameOver == false)
        {
            currentRot.x -= Input.GetAxis("Mouse Y") * 180f * Time.deltaTime;
            currentRot.x = Mathf.Clamp(currentRot.x, -45f, 45f);
            transform.rotation = Quaternion.Euler(currentRot.x, player.eulerAngles.y, player.eulerAngles.z);
        }        
    }

    #endregion
}
